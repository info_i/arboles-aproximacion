#!/usr/bin/env pythoh3

'''
Cálculo del número óptimo de árboles.
'''

import sys

base_trees = 0
fruit_per_tree = 0
reduction = 0


def compute_trees(trees):

    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production


def compute_all(min_trees, max_trees):

    productions = []
    best_production = 0
    best_trees = 0
    for i in range(min_trees, max_trees + 1):
        production = compute_trees(i)
        productions.append((i, production))

        if production > best_production:
            best_production = production
            best_trees = i

    return productions, best_production, best_trees


def read_arguments():
    if len(sys.argv) != 6:
        sys.exit('Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>')

    try:
        base_trees_f = int(sys.argv[1])
        fruit_per_tree_f = int(sys.argv[2])
        reduction_f = int(sys.argv[3])
        min_val = int(sys.argv[4])
        max_val = int(sys.argv[5])
    except ValueError:
        sys.exit("All arguments must be integers")

    return base_trees_f, fruit_per_tree_f, reduction_f , min_val, max_val


def main():

    global base_trees, fruit_per_tree, reduction
    base_trees, fruit_per_tree, reduction, min_val, max_val = read_arguments()
    productions, best_production, best_trees = compute_all(min_val, max_val)

    for trees, production in productions:
        print(f'{trees} {production}')

    print(f"Best production: {best_production}, for {best_trees} trees")


if __name__ == '__main__':
    main()
